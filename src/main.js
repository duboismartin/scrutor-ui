import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import Axios from 'axios'
import Vuetify from "vuetify";

//Helpers
import colors from 'vuetify/es5/util/colors'

Vue.config.productionTip = true

Vue.prototype.$http = Axios;
const token = localStorage.getItem('token');
if(token) {
  Vue.prototype.$http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
}

Vue.use(Vuetify, {
  theme: {
    primary: colors.red.darken1,
    secondary: colors.red.lighten4,
    accent: colors.indigo.base
  }
})

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
    if(store.getters.isLoggedIn) {
      next()
      return
    }
    next('/login')
  }else{
    next()
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
