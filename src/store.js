import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        status: '',
        token: localStorage.getItem('token') || '',
        user: {},
        //PROJECTS
        projects: [],
        processes: [],
        logReports: []
    },
    mutations: {
        auth_request(state){
            state.status = 'loading'
        },
        auth_success(state, token, user){
            state.status = 'success'
            state.token = token
            state.user = user
        },
        auth_error(state){
            state.status = 'error'
        },
        logout(state){
            state.status = ''
            state.token = ''
            state.projects = []
        },
        setProjects(state, projects){
            state.projects = projects
        },
        setProcesses(state, processes, projectId){
            state.processes[projectId] = processes
        },
        setLogReports(state, logReports, processId, date = new Date().toISOString().substr(0, 10)){
            state.logReports[processId] = logReports
        }
    },
    actions: {
        login({commit}, user){
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios({url: 'https://vps.edt.ovh/api/auth/sign_in', data: user, method: 'POST'})
                    .then(resp => {
                        const token = resp.data.token
                        const user = resp.data.user
                        localStorage.setItem('token', token)
                        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
                        commit('auth_success', token, user)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error');
                        localStorage.removeItem('token')
                        delete axios.defaults.headers.common['Authorization']
                        reject(err)
                    })
            })
        },
        register({commit}, user){
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios({url: 'https://vps.edt.ovh/api/auth/register', data: user, method: 'POST'})
                    .then(resp => {
                        const token = resp.data.token
                        const user = resp.data.user
                        localStorage.setItem('token', token)
                        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
                        commit('auth_success', token, user)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error', err)
                        localStorage.removeItem('token')
                        delete axios.defaults.headers.common['Authorization']
                        reject(err)
                    })
            })
        },
        logout({commit}){
            return new Promise((resolve, reject) => {
                commit('logout')
                localStorage.removeItem('token')
                delete axios.defaults.headers.common['Authorization']
                resolve()
            })
        },
        fetchProjects({commit, state}){
            return new Promise((resolve, reject) => {
                if(!!state.token){
                    /*if(state.projects != []){
                        resolve(state.projects);
                    }else{*/
                        axios({url: 'https://vps.edt.ovh/api/project', method: 'GET'})
                            .then(resp => {
                                commit('setProjects', resp.data.projects);
                                resolve(resp.data.projects);
                            })
                            .catch(err => {
                                reject(err.response);
                            })
                    //}
                }else{
                    commit('setProjects', []);
                    reject("Not logged");
                }
            })
        },
        fetchProcesses({commit, state}, projectId){
            return new Promise((resolve, reject) => {
                if(!!state.token){
                    axios({url: 'https://vps.edt.ovh/api/project/'+projectId+'/processes', method: 'GET'})
                        .then(resp => {
                            commit('setProcesses', resp.data.processes, projectId);
                            resolve(resp.data.processes);
                        })
                        .catch(err => {
                            reject(err.response);
                        })
                }else{
                    commit('setProcesses', [], projectId);
                    reject("Not logged");
                }
            })
        },
        fetchLogReports({commit, state}, processId){
            return new Promise((resolve, reject) => {
                if(!!state.token){
                    axios({url: 'https://vps.edt.ovh/api/process/'+processId+'/logReports', method: 'GET'})
                        .then(resp => {
                            commit('setLogReports', resp.data.logReports, processId);
                            resolve(resp.data.logReports);
                        })
                        .catch(err => {
                            reject(err.response);
                        })
                }else{
                    commit('setLogReports', [], processId);
                    reject("Not logged");
                }
            })
        },
        fetchLogReportsForToday({commit, state}, processId){
            return new Promise((resolve, reject) => {
                if(!!state.token){
                    axios({url: 'https://vps.edt.ovh/api/process/'+processId+'/logReports/byDate/', method: 'GET'})
                        .then(resp => {
                            commit('setLogReports', resp.data.logReports, processId);
                            resolve(resp.data.logReports);
                        })
                        .catch(err => {
                            reject(err.response);
                        })
                }else{
                    commit('setLogReports', [], processId);
                    reject("Not logged");
                }
            })
        },
        fetchLogReportsForADay({commit, state}, [processId, date]){
            return new Promise((resolve, reject) => {
                if(!!state.token){
                    window.console.log(date);
                    axios({url: 'https://vps.edt.ovh/api/process/'+processId+'/logReports/byDate/'+date, method: 'GET'})
                        .then(resp => {
                            commit('setLogReports', resp.data.logReports, processId, date);
                            resolve(resp.data.logReports);
                        })
                        .catch(err => {
                            reject(err.response);
                        })
                }else{
                    commit('setLogReports', [], processId);
                    reject("Not logged");
                }
            })
        }
    },
    getters: {
        isLoggedIn: state => !!state.token,
        authStatus: state => state.status,
        projects: state => state.projects,
        processes: state => state.processes,
        logReports: state => state.logReports
    }
})
