import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/components/Login'
import Register from '@/components/Register'
import Home from '@/components/Home'
import ProjectDetails from "@/components/ProjectDetails"
import ProcessDetails from "@/components/ProcessDetails"

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/home',
            name: 'home',
            component: Home,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/:projectId',
            name: 'projectDetails',
            component: ProjectDetails,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/:projectId/:processId',
            name:'processDetails',
            component: ProcessDetails,
            meta: {
                requiresAuth: true
            }
        }
    ]
})
